#**CoppeTex**#

This is a LaTeX toolkit for writing thesis and dissertations following COPPE's
directives. Since the [orginal webpage in sourceforge](http://coppetex.sourceforge.net/index.html) seems 
to be abandond, I'm bringing the code here, to GitLab.

From what I could understand, the original project members are:
 - Vicente H. F. Batista [Editor-in-Chief]
 - George O. Ainsworth Jr. [Developer]
 - Paulo Laranjeira da C. Lage [Chair]

And was last modified by someone that signs as *Helano* in 2011.

All credit goes to those guys, I just brought the entire code here from
[source forge](http://coppetex.sourceforge.net/), and tried to solve same 
small issues.